from django.apps import AppConfig


class DesktopConfig(AppConfig):
    name = 'DESKTOP'
